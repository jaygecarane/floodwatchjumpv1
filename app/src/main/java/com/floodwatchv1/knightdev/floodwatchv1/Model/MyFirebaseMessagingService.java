package com.floodwatchv1.knightdev.floodwatchv1.Model;

import android.content.Intent;
import android.util.Log;

import com.floodwatchv1.knightdev.floodwatchv1.Controller.Login;
import com.floodwatchv1.knightdev.floodwatchv1.Controller.MapAndList;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by knightdev on 5/15/17.
 */

public class

MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        String title = null;
        String message = null;
        try {
            title = remoteMessage.getData().get("title");
        } catch (Exception e) {
            title = remoteMessage.getNotification().getTitle().toString();
        }
//        if (title==null)title = remoteMessage.getNotification().getTitle().toString();
        try {
            message = remoteMessage.getData().get("message");
        } catch (Exception e) {
            message = remoteMessage.getNotification().getBody().toString();
        }
//        if (message==null)message = remoteMessage.getNotification().getBody().toString();


        Log.i("Title", "Message received [" + title + "]");
        Log.i("Msg", "Message received " + message + "");

        sendPushNotification(title, message);

    }
    private void sendPushNotification(String title,String message) {
        try {
//
            MyNotificationManager mNotificationManager = new MyNotificationManager(getApplicationContext());

            //creating an intent for the notification
            Intent intent = new Intent(getApplicationContext(), MapAndList.class);
                mNotificationManager.showSmallNotification(title, message, intent);
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }
}

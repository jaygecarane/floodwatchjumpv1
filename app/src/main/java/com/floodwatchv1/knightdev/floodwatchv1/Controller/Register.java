package com.floodwatchv1.knightdev.floodwatchv1.Controller;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.floodwatchv1.knightdev.floodwatchv1.BuildConfig;
import com.floodwatchv1.knightdev.floodwatchv1.Model.DataManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.SharedPrefManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.User;
import com.floodwatchv1.knightdev.floodwatchv1.R;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Constants;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Interfaces;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.RequestInterface;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerRequest;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Register extends AppCompatActivity implements View.OnTouchListener{
    TextView label,back_login;
    EditText et_lastname,et_firstname,et_email,et_username,et_password,et_confirmPassword;
    String lastname ="",firstname="",email="",username="",password="",finalPassword="",dev_token="";
    Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        Typeface face= Typeface.createFromAsset(getAssets(),"fonts/Sanchez-Regular.ttf");
        label = (TextView)findViewById(R.id.tv_title_lbl3);
        back_login = (TextView)findViewById(R.id.tv_back_to_login);
        //et_lastname = (EditText)findViewById(R.id.et_lastname);
        //et_firstname = (EditText)findViewById(R.id.et_firstname);
        et_email = (EditText)findViewById(R.id.et_email);
        //et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText)findViewById(R.id.et_password);
        et_confirmPassword = (EditText)findViewById(R.id.et_confirmPassword);

        register = (Button)findViewById(R.id.btn_register);

        label.setTypeface(face);
        back_login.setTypeface(face);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        register.setOnTouchListener(this);
        back_login.setOnTouchListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), Login.class);
        startActivityForResult(myIntent, 0);
        finish();

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(Register.this,Login.class));
        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //lastname = et_lastname.getText().toString();
        //firstname = et_firstname.getText().toString();
        email = et_email.getText().toString();
        //username = et_username.getText().toString();
        password = et_password.getText().toString();
        finalPassword = et_confirmPassword.getText().toString();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log.d(Constants.TAG,refreshedToken);
        storeToken(refreshedToken);
        String token = SharedPrefManager.getInstance(this).getDeviceToken();
        dev_token = token;
        switch(event.getAction()){
            case MotionEvent.ACTION_UP:
                if (v.getId()==R.id.tv_back_to_login){
                    startActivity(new Intent(Register.this,Login.class));
                    finish();
                }else if (v.getId() ==R.id.btn_register){
                    if (password.isEmpty() && finalPassword.isEmpty() && email.isEmpty()) {
                        et_email.setError("This field should not be empty");
                        et_password.setError("This field should not be empty");
                        et_confirmPassword.setError("This field should not be empty");
                        // Toast.makeText(getApplicationContext(),"Parameters should not be empty !",Toast.LENGTH_LONG).show();
                    }else if (email.isEmpty()){
                        et_email.setError("This field should not be empty");
                    }else if (email.isEmpty()&& password.isEmpty()){
                        et_email.setError("This field should not be empty");
                        et_password.setError("This field should not be empty");
                        et_confirmPassword.setError("This field should not be empty");
                    }else if (password.isEmpty()&& finalPassword.isEmpty()){
                        et_password.setError("This field should not be empty");
                        et_confirmPassword.setError("This field should not be empty");
                    }else if (finalPassword.isEmpty()){
                        et_confirmPassword.setError("This field should not be empty");
                    }else if (!password.equals(finalPassword)){
                        et_password.setError("Your passwords does not match");
                        et_confirmPassword.setError("Your passwords does not match");
                    } else if (password.equals(finalPassword)){
                      registerProcess(email,password,finalPassword,dev_token);
                     // Toast.makeText(this,email+" "+password+" "+finalPassword+" "+dev_token,Toast.LENGTH_SHORT ).show();
                         //loading = ProgressDialog.show( this,"Connecting...","Please wait...",false,false);

                        /*User user = new User();
                        user.setLastName(lastname);
                        user.setFirstName(firstname);
                        user.setUserName(username);
                        user.setEmail(email);
                        user.setPassword(password);
                        user.setToken(dev_token);
                        ServerRequest request = new ServerRequest();
                        request.setOperation(Constants.REGISTER_OPERATION);
                        request.setUser(user);
                        DataManager.getInstance(this).registerProcess(request,
                        new Interfaces.RegisterCallback() {
                            @Override
                            public void onResponse(boolean isSuccess, ServerResponse response, String message) {
                                ServerResponse resp = response;

                                if (isSuccess){
                                    if(resp.getResult().equals(Constants.SUCCESS)){
                                        Toast.makeText(getApplicationContext(),"Registered Successfully",Toast.LENGTH_SHORT).show();
                                        try {
                                            if (resp.getMessage().equals("User Registered Successfully !")) {
                                                if (resp.getResult().equals(Constants.SUCCESS)) {
                                                    Toast.makeText(getApplicationContext(), "Registered Successfully", Toast.LENGTH_SHORT).show();
                                                    startActivity(new Intent(getApplicationContext(), Login.class));
                                                    finish();
                                                }
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Email has already been taken", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch(Exception e){
                                            Toast.makeText(getApplicationContext(), "Email has already been taken", Toast.LENGTH_SHORT).show();
                                        }

                                        startActivity(new Intent(getApplicationContext(),Login.class));
                                        finish();
                                    }
                                }else {
                                    displayAlert(resp.getMessage().toString());
                                }

                                loading.dismiss();
                            }
                        });*/
                    }else{
                        Toast.makeText(getApplicationContext(),"Passwords does not match",Toast.LENGTH_LONG).show();
                    }
                }
        }
        return true;
    }


    private void registerProcess(String email,String password, String final_password,String token){

   // private void registerProcess(String lastname,String firstname,String username, String email,String password,String le_token){
        final ProgressDialog loading = ProgressDialog.show(this,"Connecting...","Please wait...",false,false);

        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);

        //builder.certificatePinner(new CertificatePinner.Builder().add("*.androidadvance.com", "sha256/RqzElicVPA6LkKm9HblOvNOUqWmD+4zNXcRb+WjcaAE=")
        //    .add("*.xxxxxx.com", "sha256/8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8=")
        //    .add("*.xxxxxxx.com", "sha256/Ko8tivDrEjiY90yGasP6ZpBU4jwXvHqVvQI0GS3GNdA=")
        //    .add("*.xxxxxxx.com", "sha256/VjLZe/p3W/PJnd6lL8JVNBCGQBZynFLdZSTIqcO0SJ8=")
        //    .build());

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(this.getCacheDir(), cacheSize);
        builder.cache(cache);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);

        User user = new User();
        //user.setLastName(lastname);
        //user.setFirstName(firstname);
        //user.setUserName(username);
        user.setEmail(email);
        user.setPassword(password);
        user.setFinalPassword(final_password);
        user.setToken(token);

        ServerRequest request = new ServerRequest();
        //request.setOperation(Constants.REGISTER_OPERATION);
        request.setEmail(email);
        request.setPassword(password);
        request.setPasswordConfirmation(final_password);
        request.setToken(token);

        Call<ServerResponse> response = requestInterface.register(request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {
                try {
                    ServerResponse resp = response.body();
                    if (resp.getMessage().equals("User Registered Successfully !")) {
                        if (resp.getResult().equals(Constants.SUCCESS)) {
                            loading.dismiss();
                            Toast.makeText(getApplicationContext(), "Registered Successfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), Login.class));
                            finish();
                        }
                    } else {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), "Email has already been taken", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    loading.dismiss();
                    Toast.makeText(getApplicationContext(), "Server Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                //progress.setVisibility(View.INVISIBLE);
                Log.d(Constants.TAG,t.getLocalizedMessage());
                //Toast.makeText(getApplicationContext(),t.getLocalizedMessage() ,Toast.LENGTH_SHORT).show();
                //Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
                loading.dismiss();
                Toast.makeText(getApplicationContext(),"Connection Error",Toast.LENGTH_SHORT).show();

            }
        });
    }
    private void storeToken(String token) {
        //we will save the token in sharedpreferences later
        SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(token);
    }

}

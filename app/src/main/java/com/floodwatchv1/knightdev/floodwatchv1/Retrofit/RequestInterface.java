package com.floodwatchv1.knightdev.floodwatchv1.Retrofit;

/**
 * Created by knightdev on 5/9/17.
 */

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface RequestInterface {

        //@POST("retrofit/index.php")
        //Call<ServerResponse> operation(@Body ServerRequest request);
        @POST("api/users")
        Call<ServerResponse> register(@Body ServerRequest request);

        @POST("api/users/login")
        Call<ServerResponse> login(@Header("Content-Type") String content, @Header("Token") String token, @Body ServerRequest request);

        @POST("api/users/change")
        Call<ServerResponse> password(@Header("Content-Type") String content, @Header("Token") String token, @Body ServerRequest request);

        @POST("api/users/update")
        Call<ServerResponse> profile(@Header("Content-Type") String content, @Header("Token") String token, @Body ServerRequest request);

        @POST("api/users/logout")
        Call<ServerResponse> logout(@Header("Content-Type") String content, @Header("Token") String token, @Body ServerRequest request);

}


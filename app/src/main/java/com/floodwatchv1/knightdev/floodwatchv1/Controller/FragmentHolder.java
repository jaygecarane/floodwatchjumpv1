package com.floodwatchv1.knightdev.floodwatchv1.Controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;


import com.floodwatchv1.knightdev.floodwatchv1.Model.Communicator;
import com.floodwatchv1.knightdev.floodwatchv1.Model.SessionManager;
import com.floodwatchv1.knightdev.floodwatchv1.R;


/**
 * Created by knightdev on 6/14/17.
 */

public class FragmentHolder extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ViewPager viewPager;
    SessionManager session;
    AlertDialog.Builder builder;
    MenuItem search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_holder);
        initViews();
    }

    public void initViews() {

        builder = new AlertDialog.Builder(this);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();

        viewPager = (ViewPager) findViewById(R.id.view_pager1);
        Communicator communicator = new Communicator(getSupportFragmentManager());
        viewPager.setAdapter(communicator);
        viewPager.setCurrentItem(0);
        drawerLayout = (DrawerLayout) findViewById(R.id.dl_main);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    Bundle bundle = new Bundle();

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_id: {
                Fragment fragment = new MapAndList();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_to_left, R.anim.exit_to_right);
                transaction.replace(R.id.content_frame, fragment);
                transaction.commit();
                search.setVisible(true);
                break;
            }
            case R.id.changePass_id: {
                Fragment fragment = new ChangePassword();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_to_left, R.anim.exit_to_right);
                transaction.replace(R.id.content_frame, fragment);
                transaction.commit();
                search.setVisible(false);
                break;
            }
            case R.id.updateProfile_id: {
                Fragment fragment = new UpdateProfile();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_to_left, R.anim.exit_to_right);
                transaction.replace(R.id.content_frame, fragment);
                transaction.commit();
                search.setVisible(false);
                break;
            }
            case R.id.logout_id: {
                session.logoutUser();
                startActivity(new Intent(FragmentHolder.this, Login.class));
                finish();
                break;
            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        displayAlert("Do you really want to exit?");

    }

    private void displayAlert(final String message) {
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;

    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    MapAndList.mAdapter.getFilter().filter(newText.toLowerCase());
                } catch (Exception e) {

                }
                return true;
            }
        });
    }
}

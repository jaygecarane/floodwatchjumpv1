package com.floodwatchv1.knightdev.floodwatchv1.Controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.floodwatchv1.knightdev.floodwatchv1.BuildConfig;
import com.floodwatchv1.knightdev.floodwatchv1.MainContent;

import com.floodwatchv1.knightdev.floodwatchv1.Model.CustomMessageEvent;
import com.floodwatchv1.knightdev.floodwatchv1.Model.User;
import com.floodwatchv1.knightdev.floodwatchv1.R;

import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Constants;

import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.RequestInterface;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerRequest;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerResponse;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GoogleMap extends AppCompatActivity implements View.OnTouchListener {
    Button go_to_login, search_location;
    EditText search_location_field;
    String location = "",emailCon="",lastnameCon="",firstnameCon="",usernameCon="",uniqueIDCon="",
                        passwordCon="", token="";
    Button btnAll;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_map);

        btnAll = (Button) findViewById(R.id.btn_hide);
        search_location_field = (EditText) findViewById(R.id.et_search_location_user);
        search_location = (Button) findViewById(R.id.btn_search_location_user);
        // setUpMapIfNeeded();
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.mainlogo_ic);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        getSharedPreferences("MyPref", 0).edit().clear().commit();
        //int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        search_location = (Button) findViewById(R.id.btn_search_location_user);

        search_location_field = (EditText) findViewById(R.id.et_search_location_user);

        search_location.setOnTouchListener(this);
        btnAll.setOnTouchListener(this);

        Bundle bundle = getIntent().getExtras();

//        lastnameCon=bundle.getString("last_name");
//        firstnameCon=bundle.getString("first_name");
        emailCon=bundle.getString("email");
        usernameCon=bundle.getString("username");
        passwordCon=bundle.getString("password");
        uniqueIDCon=bundle.getString("uniqueID");
        token = bundle.getString("token");

//        lastnameCon = bundle.getString("lastname");
//        firstnameCon = bundle.getString("firstname");
//        emailCon = bundle.getString("email");
//        usernameCon = bundle.getString("username");
//        passwordCon = bundle.getString("password");
//        uniqueIDCon = bundle.getString("uniqueID");

//        Toast.makeText(this,
//                lastnameCon+"\n"+
//                firstnameCon+"\n"+
//                emailCon+"\n"+
//                usernameCon+"\n"+
//                passwordCon+"\n"+
//                uniqueIDCon,Toast.LENGTH_LONG).show();

    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(GoogleMap.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_items, menu);
//        return true;
//    }

 //   @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            // action with ID action_refresh was selected
//            case R.id.action_change_password:
//                Intent intent = new Intent(getApplicationContext(), ChangePassword.class);
//                Bundle bundle = new Bundle();
//
//                bundle.putString("email",emailCon);
////                bundle.putString("last_name", lastnameCon);
////                bundle.putString("first_name", firstnameCon);
//                bundle.putString("username", usernameCon);
//                bundle.putString("password", passwordCon);
//                bundle.putString("uniqueID", uniqueIDCon);
//                bundle.putString("token", token);
//                intent.putExtras(bundle);
//                startActivity(intent);
//                break;
//            // action with ID action_settings was selected
//
//            case R.id.action_update_profile:
//                Intent intent1 = new Intent(getApplicationContext(),UpdateProfile.class);
//                Bundle bundle1 = new Bundle();
//                bundle1.putString("email",emailCon);
////                bundle1.putString("last_name", lastnameCon);
////                bundle1.putString("first_name", firstnameCon);
//                bundle1.putString("username", usernameCon);
//                bundle1.putString("password", passwordCon);
//                bundle1.putString("uniqueID", uniqueIDCon);
//                bundle1.putString("token", token);
//                intent1.putExtras(bundle1);
//                startActivity(intent1);
//                finish();
//                break;
//            case R.id.action_logOut:
//
//                final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
//                builder.readTimeout(60, TimeUnit.SECONDS);
//                builder.connectTimeout(60, TimeUnit.SECONDS);
//                builder.writeTimeout(60, TimeUnit.SECONDS);
//
//                if (BuildConfig.DEBUG) {
//                    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//                    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//                    builder.addInterceptor(interceptor);
//                }
//                int cacheSize = 30 * 1024 * 1024; // 10 MiB
//                Cache cache = new Cache(this.getCacheDir(), cacheSize);
//                builder.cache(cache);
//                Retrofit retrofit = new Retrofit.Builder()
//                        .baseUrl(Constants.BASE_URL)
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build();
//
//
//                User user = new User();
//                user.setToken(token);
//
//                ServerRequest request = new ServerRequest();
//
//                request.setToken(token);
//
//                Toast.makeText(this, token, Toast.LENGTH_SHORT).show();
//                RequestInterface requestInterface = retrofit.create(RequestInterface.class);
//
//                Call<ServerResponse> response = requestInterface.logout("application/json", token, request);
//
//                response.enqueue(new Callback<ServerResponse>() {
//                    @Override
//                    public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {
//                        try {
//                            ServerResponse resp = response.body();
//                            if (resp.getMessage().equals("Logged-out! !")) {
//                                if (resp.getResult().equals(Constants.SUCCESS)) {
//                                    Toast.makeText(getApplicationContext(), resp.getMessage(), Toast.LENGTH_SHORT).show();
//                                    startActivity(new Intent(getApplicationContext(), Login.class));
//                                    finish();
//                                }
//                            } else {
//                                Toast.makeText(getApplicationContext(), "Try Again!", Toast.LENGTH_SHORT).show();
//                            }
//                        } catch(Exception e){
//                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<ServerResponse> call, Throwable t) {
//
//                        //progress.setVisibility(View.INVISIBLE);
//                        Log.d(Constants.TAG,"failed");
//                        //Toast.makeText(getApplicationContext(),t.getLocalizedMessage() ,Toast.LENGTH_SHORT).show();
//                        //Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
//                        Toast.makeText(getApplicationContext(),"Connection Error",Toast.LENGTH_SHORT).show();
//
//                    }
//                });
//                break;
//            default:
//                break;
//        }
//
//        return true;
//    }

    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_UP:
                if (v.getId() == R.id.btn_search_location_user) {
                    location = search_location_field.getText().toString();
                    CustomMessageEvent event = new CustomMessageEvent();
                    event.setCustomMessage(location.toString());
                    EventBus.getDefault().post(event);
                } else if (v.getId() == R.id.btn_hide) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
        }
        return true;
    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        menu.findItem(R.id.action_map).setVisible(false);
//        super.onPrepareOptionsMenu(menu);
//        return true;
//    }

}

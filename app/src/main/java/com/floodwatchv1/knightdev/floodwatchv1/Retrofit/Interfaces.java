package com.floodwatchv1.knightdev.floodwatchv1.Retrofit;


/**
 * Created by knightdev on 5/5/17.
 */

public class Interfaces {

    public interface RegisterCallback{
        void onResponse(boolean isSuccess, ServerResponse response, String message);
    }
    public interface LoginCallback{
        void onResponse(boolean isSuccess, ServerResponse response, String message);
    }
    public interface ChangePassCallback{
        void onResponse(boolean isSuccess, ServerResponse response, String message);
    }
    public interface UpdateProfileCallback{
        void onResponse(boolean isSuccess, ServerResponse response, String message);
    }
}

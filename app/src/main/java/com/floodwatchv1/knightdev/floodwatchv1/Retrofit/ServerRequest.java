package com.floodwatchv1.knightdev.floodwatchv1.Retrofit;

import com.floodwatchv1.knightdev.floodwatchv1.Model.User;

/**
 * Created by knightdev on 5/9/17.
 */

public class ServerRequest {
    private String operation;
    private String email;
    private String username;
    private String first_name;
    private String last_name;
    private String password;
    private String password_confirmation;
    private String new_password;
    private String newpassword;
    private String newpassword_confirmation;

    private String device_token;
    private User user;

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFirstname(String first_name) {
        this.first_name = first_name;
    }

    public void setLastname(String last_name) {
        this.last_name = last_name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPasswordConfirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public void setToken(String device_token) {
        this.device_token = device_token;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public void setNewpassword_confirmation(String newpassword_confirmation) {
        this.newpassword_confirmation = newpassword_confirmation;
    }
}

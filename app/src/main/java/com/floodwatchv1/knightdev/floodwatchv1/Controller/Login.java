package com.floodwatchv1.knightdev.floodwatchv1.Controller;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.floodwatchv1.knightdev.floodwatchv1.BuildConfig;

import com.floodwatchv1.knightdev.floodwatchv1.Model.SessionManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.SharedPrefManager;

import com.floodwatchv1.knightdev.floodwatchv1.Model.DataManager;

import com.floodwatchv1.knightdev.floodwatchv1.Model.User;
import com.floodwatchv1.knightdev.floodwatchv1.R;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Constants;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Interfaces;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.RequestInterface;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerRequest;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity implements View.OnTouchListener {

    String email2 = "", password = "", dev_token = "";
    TextView title_1, title_2, to_register, showForgotPassword;
    EditText et_email2, et_password;

    Button login_user;
    AlertDialog.Builder builder;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        storeToken(refreshedToken);
        String token = SharedPrefManager.getInstance(this).getDeviceToken();
        dev_token = token;

        session = new SessionManager(getApplicationContext());
        builder = new AlertDialog.Builder(Login.this);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Sanchez-Regular.ttf");
        title_1 = (TextView) findViewById(R.id.tv_title_lbl1);
        title_2 = (TextView) findViewById(R.id.tv_title_lbl2);
        to_register = (TextView) findViewById(R.id.tv_register);
        showForgotPassword = (TextView) findViewById(R.id.tv_forgotPassword);

        login_user = (Button) findViewById(R.id.btn_login);

        et_email2 = (EditText) findViewById(R.id.et_email2);
        et_password = (EditText) findViewById(R.id.et_password);

        title_1.setTypeface(face);
        title_2.setTypeface(face);
        to_register.setTypeface(face);
        showForgotPassword.setTypeface(face);

        to_register.setOnTouchListener(this);
        login_user.setOnTouchListener(this);
        showForgotPassword.setOnTouchListener(this);

    }

    @Override
    public void onBackPressed() {
        displayAlert("Do you really want to exit?");

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        email2 = et_email2.getText().toString();
        password = et_password.getText().toString();
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (v.getId() == R.id.tv_register) {
                    startActivity(new Intent(Login.this, Register.class));
                    finish();
                } else if (v.getId() == R.id.btn_login) {

                    if (!email2.isEmpty() && !password.isEmpty()) {
                        Toast.makeText(this, "Logging in...", Toast.LENGTH_LONG).show();
                        loginProcess(email2, password, dev_token);
                    } else if (password.isEmpty() && email2.isEmpty()) {
                        et_password.setError("This field should not be empty!");
                        et_email2.setError("This field should not be empty!");
                    } else if (email2.isEmpty()) {
                        et_email2.setError("This field should not be empty!");
                        //Toast.makeText(getApplicationContext(),"Parameters should not be empty !" ,Toast.LENGTH_SHORT).show();
                    } else if (password.isEmpty()) {
                        et_password.setError("This field should not be empty!");
                    }
                    //progress.setVisibility(View.VISIBLE);

                    /*loading = ProgressDialog.show(this, "Connecting...", "Please wait...", false, false);
                    Toast.makeText(this, "Logging in...", Toast.LENGTH_LONG).show();
                    //progress.setVisibility(View.VISIBLE);
                    //loginProcess(email2, password);
                    User user = new User();
                    user.setEmail(email2);
                    user.setPassword(password);
                    ServerRequest request = new ServerRequest();
                    request.setOperation(Constants.LOGIN_OPERATION);
                    request.setUser(user);
                    DataManager.getInstance(this).loginProcess(request,
                            new Interfaces.LoginCallback() {
                                @Override
                                public void onResponse(boolean isSuccess, ServerResponse response, String message) {
                                    if (isSuccess) {
                                        if (response.getResult().equals(Constants.SUCCESS)) {
                                            loading.dismiss();
                                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
                                            SharedPreferences.Editor editor = pref.edit();
                                            editor.putBoolean(Constants.IS_LOGGED_IN, true);
                                            editor.putString(Constants.EMAIL, response.getUser().getEmail());
                                            editor.putString(Constants.LASTNAME, response.getUser().getLastName());
                                            editor.putString(Constants.FIRSTNAME, response.getUser().getFirstName());
                                            editor.putString(Constants.USERNAME, response.getUser().getUserName());
                                            editor.putString(Constants.UNIQUE_ID, response.getUser().getUnique_id());
                                            editor.putString(Constants.PASSWORD, response.getUser().getPassword());
                                            editor.putString(Constants.UNIQUE_ID, response.getUser().getUnique_id());
                                            editor.apply();
                                            editor.commit();
                                            Intent intent = new Intent(Login.this, GoogleMap.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("email", response.getUser().getEmail());
                                            bundle.putString("lastname", response.getUser().getLastName());
                                            bundle.putString("firstname", response.getUser().getFirstName());
                                            bundle.putString("username", response.getUser().getUserName());
                                            bundle.putString("password", password.toString());
                                            bundle.putString("uniqueID", response.getUser().getUnique_id());
                                            intent.putExtras(bundle);
                                            startActivity(intent);
                                            finish();
                                        }
                                        else {
                                            loading.dismiss();
                                            displayAlert(response.getMessage().toString());
                                        }
                                    }
                                }
                            });*/

                } else if (v.getId() == R.id.tv_forgotPassword) {
                    /*LayoutInflater li = LayoutInflater.from(context);
                    View promptsView = li.inflate(R.layout.prompt, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            context);

                    // set prompts.xml to alertdialog builder
                    alertDialogBuilder.setView(promptsView);

                    final EditText userInput = (EditText) promptsView
                            .findViewById(R.id.editTextDialogUserInput);

                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getApplicationContext(),userInput.getText() ,Toast.LENGTH_SHORT).show();
                        }
                    })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();*/
                    Intent intent = new Intent(this, ForgotPassword.class);
                    startActivity(intent);
                }
        }

        return true;
    }


    private void loginProcess(final String email, final String password, final String token) {
        final ProgressDialog loading = ProgressDialog.show(this, "Connecting...", "Please wait...", false, false);

        //private void loginProcess(String email, final String password) {


        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);

        //builder.certificatePinner(new CertificatePinner.Builder().add("*.androidadvance.com", "sha256/RqzElicVPA6LkKm9HblOvNOUqWmD+4zNXcRb+WjcaAE=")
        //    .add("*.xxxxxx.com", "sha256/8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8=")
        //    .add("*.xxxxxxx.com", "sha256/Ko8tivDrEjiY90yGasP6ZpBU4jwXzvHqVvQI0GS3GNdA=")
        //    .add("*.xxxxxxx.com", "sha256/VjLZe/p3W/PJnd6lL8JVNBCGQBZynFLdZSTIqcO0SJ8=")
        //    .build());

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(this.getCacheDir(), cacheSize);
        builder.cache(cache);


        Retrofit retrofit = new Retrofit.Builder()
                .client(builder.build())
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setToken(token);

        ServerRequest request = new ServerRequest();
        //request.setOperation(Constants.LOGIN_OPERATION);
        //request.setUser(user);
        request.setEmail(email);
        request.setPassword(password);
        request.setToken(token);

        Call<ServerResponse> response = requestInterface.login("application/json", "", request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {

                ServerResponse resp = response.body();
                //Toast.makeText(getApplicationContext(),resp.getMessage() ,Toast.LENGTH_LONG).show();
                //Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();
                try {
                    if (resp.getMessage().equals("User Logged-in Successfully !")) {
                        if (resp.getResult().equals(Constants.SUCCESS)) {
                            loading.dismiss();

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);

//                            SharedPreferences.Editor editor = pref.edit();
//                            editor.putBoolean(Constants.IS_LOGGED_IN, true);
//                            editor.putString(Constants.EMAIL, email2);
//                            editor.putString(Constants.PASSWORD, resp.getPassword());
//                            editor.putString(Constants.LASTNAME, resp.getLastName());
//                            editor.putString(Constants.FIRSTNAME, resp.getFirstName());
//                            editor.putString(Constants.USERNAME, resp.getUserName());
//                            editor.putString(Constants.TOKEN, resp.getToken());
//                            //editor.putString(Constants.UNIQUE_ID,resp.getUser().getUnique_id());
//                            editor.apply();
//                            editor.commit();
//                            //startActivity(new Intent(getApplicationContext(),GoogleMap.class));
                            session.createLoginSession(email2, resp.getToken());
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("email", email2);
                            editor.putString("token", resp.getToken());
                            editor.commit();
                            startActivity(new Intent(getApplicationContext(),FragmentHolder.class));
//                            Toast.makeText(getApplicationContext(),resp.getToken().toString(),Toast.LENGTH_SHORT).show();
                        }
                    } else if (resp.getMessage().equals("Parameters should not be empty !")) {
                        displayAlert(resp.getMessage().toString());
                    } else if (resp.getMessage().equals("User Already Logged-in!")) {
                        displayAlert(resp.getMessage().toString());
                    } else if (resp.getMessage().equals(" Invalid Email!")) {
                        displayAlert(resp.getMessage().toString());
                    } else if (resp.getMessage().equals("Incorrect Password!")) {
                        displayAlert(resp.getMessage().toString());
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Connection/Server Error", Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(),t.getLocalizedMessage() ,Toast.LENGTH_SHORT).show();
                //Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

            }
        });
    }

    private void displayAlert(final String message) {
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void storeToken(String token) {
        //we will save the token in sharedpreferences later
        SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(token);
    }

}

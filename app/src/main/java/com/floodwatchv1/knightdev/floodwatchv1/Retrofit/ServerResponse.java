package com.floodwatchv1.knightdev.floodwatchv1.Retrofit;

import com.floodwatchv1.knightdev.floodwatchv1.Model.User;

/**
 * Created by knightdev on 5/9/17.
 */

public class ServerResponse {
    private String result;
    private String message;
    private User user;
    private String email;
    private String password;
    private String password_confirmation;
    private String last_name;
    private String first_name;
    private String username;
    private  String token;
    private String email_p;


    public String getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }

    public String getEmail() {
        return email;
    }
    public String getPassword() { return password; }
    public String getPasswordConfirmation() { return password_confirmation; }
    public String getLastName() {
        return last_name;
    }
    public String getFirstName() {
        return first_name;
    }
    public String getUserName() {
        return username;
    }
    public String getToken() { return token; }
}

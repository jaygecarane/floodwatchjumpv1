package com.floodwatchv1.knightdev.floodwatchv1.Controller;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.floodwatchv1.knightdev.floodwatchv1.Model.SessionManager;
import com.floodwatchv1.knightdev.floodwatchv1.R;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.LocationData;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Constants;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.DataAdapter;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.JSONResponse;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.RequestInterfaceList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MapAndList extends Fragment {
    private RelativeLayout rlSelect, rlMap, rlList;
    private RadioGroup viewMode;
    private RadioButton mapMode, listMode;
    private RecyclerView mRecyclerView;
    private ArrayList<LocationData> mArrayList;
    private TextView nullText;
    public static DataAdapter mAdapter;

    AlertDialog.Builder builder;

    public MapAndList(){

    }


    //public static final String BASE_URL = "https://api.learn2crack.com";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.map_list_view, container, false);
        rlSelect = (RelativeLayout)view.findViewById(R.id.rl_1);
        rlMap = (RelativeLayout)view.findViewById(R.id.rl_2);
        rlList = (RelativeLayout)view.findViewById(R.id.rl_3);
        viewMode = (RadioGroup)view.findViewById(R.id.rgTask);
        mapMode = (RadioButton)view.findViewById(R.id.rbMap);
        listMode = (RadioButton)view.findViewById(R.id.rbList);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);

        changeMode();
        return view;

    }
    private void loadJSON() {
        Log.d("sample","Help");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterfaceList request = retrofit.create(RequestInterfaceList.class);
        Call<List<LocationData>> call = request.getJSON();
        call.enqueue(new Callback<List<LocationData>>() {
            @Override
            public void onResponse(Call<List<LocationData>> call, Response<List<LocationData>> response) {
                Log.d("sample","success to");
                List<LocationData>jsonResponse = response.body();
                mArrayList = new ArrayList<>(jsonResponse);
                mAdapter = new DataAdapter(mArrayList);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<List<LocationData>> call, Throwable t) {
                try {
                    Log.d("sample", "failure");
                    Log.d("Error", t.getMessage());
                }catch (Exception e){

                }
            }
        });

    }
    public void changeMode() {
        viewMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (mapMode.isChecked()) {
                    rlList.setVisibility(View.GONE);
                    rlMap.setVisibility(View.VISIBLE);
                }
                if (listMode.isChecked()) {
                    loadJSON();
                    rlList.setVisibility(View.VISIBLE);
                    rlMap.setVisibility(View.GONE);
                }
            }
        });
    }

    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}


package com.floodwatchv1.knightdev.floodwatchv1.Model;

/**
 * Created by JohnChristopher on 2016-10-11.
 */
public class CustomMessageEvent {
    private String customMessage;

    public String getCustomMessage() {
        return customMessage;
    }

    public void setCustomMessage(String customMessage) {
        this.customMessage = customMessage;
    }
}

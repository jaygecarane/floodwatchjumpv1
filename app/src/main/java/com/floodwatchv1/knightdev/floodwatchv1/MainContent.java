package com.floodwatchv1.knightdev.floodwatchv1;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.floodwatchv1.knightdev.floodwatchv1.Controller.FragmentHolder;



public class MainContent extends AppCompatActivity {
    TextView locationLbl, location, deviceIDLbl, deviceID, label4, dateLbl, date, waterLevel, label5, message;
    String locationName_con = "", deviceID_con = "", status_con = "", dateTime_con = "", uid_con = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.mainlogo_ic);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Sanchez-Regular.ttf");
        locationLbl = (TextView) findViewById(R.id.tv_location_lbl);
        location = (TextView) findViewById(R.id.tv_location);
        deviceIDLbl = (TextView) findViewById(R.id.tv_deviceID_lbl);
        deviceID = (TextView) findViewById(R.id.tv_deviceID);
        label4 = (TextView) findViewById(R.id.tv_label_4);
        dateLbl = (TextView) findViewById(R.id.tv_date_label);
        date = (TextView) findViewById(R.id.tv_date);
        waterLevel = (TextView) findViewById(R.id.tv_water_level);
        label5 = (TextView) findViewById(R.id.tv_monitor_label);
        message = (TextView) findViewById(R.id.tv_message);

        locationLbl.setTypeface(face);
        location.setTypeface(face);
        deviceIDLbl.setTypeface(face);
        deviceID.setTypeface(face);
        label4.setTypeface(face);
        dateLbl.setTypeface(face);
        date.setTypeface(face);
        waterLevel.setTypeface(face);
        label5.setTypeface(face);

        Bundle bundle = getIntent().getExtras();
        locationName_con = bundle.getString("locationName");
        status_con = bundle.getString("status");
        deviceID_con = bundle.getString("deviceID");
        dateTime_con = bundle.getString("dateTime");

        location.setText(locationName_con);
        deviceID.setText(deviceID_con);
        date.setText(" "+dateTime_con);
        waterLevel.setText(status_con);
        if (status_con.equals("EVACUATE")) {
            waterLevel.setTextColor(Color.RED);
            message.setText("The water level of this location has reach its critical limit\n" +
                    "Please evacuate immediately if you're near this location and\n" +
                    "Please notify your nearest local government branch\n" +
                    "-Flood Watch");
        } else if (status_con.equals("PREPARE")) {
            waterLevel.setTextColor(Color.YELLOW);
            message.setText("The water level of this location is above it's normal limit\n" +
                    "Please be cautious if you're near this location and\n" +
                    "Please remain calm\n" +
                    "-Flood Watch");
        } else {
            message.setText("The water level of this location is normal\n" +
                    "Nothing to worry about have a nice day!\n" +
                    "-Flood Watch");
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), FragmentHolder.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
        return;
    }


}

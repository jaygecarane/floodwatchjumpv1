package com.floodwatchv1.knightdev.floodwatchv1.Retrofit;

/**
 * Created by knightdev on 5/24/17.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.floodwatchv1.knightdev.floodwatchv1.MainContent;
import com.floodwatchv1.knightdev.floodwatchv1.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> implements Filterable {
    Context context;

    public DataAdapter(Context context) {
        this.context=context;
    }
    private ArrayList<LocationData> mArrayList;
    private ArrayList<LocationData> mFilteredList;

    public DataAdapter(ArrayList<LocationData> arrayList) {
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int i) {
        viewHolder.tv_name.setText(mFilteredList.get(i).getName());
        viewHolder.tv_time.setText(mFilteredList.get(i).getDevice().getLevel().getCreatedAt().toString());
        viewHolder.tv_api_deviceID.setText(mFilteredList.get(i).getDevice().getName().toString());
        viewHolder.tv_api_status.setText(mFilteredList.get(i).getDevice().getLevel().getStatus().toString());
    }//getDevice().getLevel().getStatus().toString();

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_time, tv_api_deviceID, tv_api_status;

        public ViewHolder(View view) {
            super(view);

            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_time = (TextView) view.findViewById(R.id.tv_time);
            tv_api_deviceID = (TextView) view.findViewById(R.id.tv_api_deviceID);
            tv_api_status = (TextView) view.findViewById(R.id.tv_api_status);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(v.getContext(),MainContent.class);
                    Bundle bundle_info = new Bundle();
                    bundle_info.putString("locationName", tv_name.getText().toString());
                    bundle_info.putString("deviceID", tv_api_deviceID.getText().toString());
                    bundle_info.putString("status", tv_api_status.getText().toString());
                    bundle_info.putString("dateTime", tv_time.getText().toString());
                    intent.putExtras(bundle_info);
                    v.getContext().startActivity(intent);

                }
            });

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<LocationData> filteredList = new ArrayList<>();

                    for (LocationData androidVersion : mArrayList) {

                        if (androidVersion.getName().toLowerCase().contains(charString) || androidVersion.getLongitude().toString().contains(charString) || androidVersion.getLatitude().toString().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<LocationData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
package com.floodwatchv1.knightdev.floodwatchv1.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by knightdev on 5/9/17.
 */

public class User {
    @SerializedName("last_name")
    private String last_name;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("username")
    private String username;

    @SerializedName("email")
    private String email;

    @SerializedName("device_token")
    private String token;
//    private String unique_id;

    @SerializedName("password")
    private String password;

    @SerializedName("password_confirmation")
    private String final_password;
    private String old_password;
    private String new_password;

    @SerializedName("newpassword")
    private String newpassword;
    @SerializedName("newpassword_confirmation")
    private String newpassword_confirmation;


    public String getLastName() {return last_name;}
    public String getFirstName() {return first_name;}

    private String newEmail;
    private String newUsername;

    public String getUserName() {return username;}
    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }


    public String getToken() {return token;}


//    public String getUnique_id() {
//        return unique_id;
//    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }
    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }
    public void setUserName(String username) { this.username = username; }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public void setFinalPassword(String final_password) {
        this.final_password = final_password;
    }

//    public void setUniqueID(String unique_id) {
//        this.unique_id = unique_id;
//    }



    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }
    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }


    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }
    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }


    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getNewpassword_confirmation() {
        return newpassword_confirmation;
    }

    public void setNewpassword_confirmation(String newpassword_confirmation) {
        this.newpassword_confirmation = newpassword_confirmation;
    }
}

package com.floodwatchv1.knightdev.floodwatchv1.Retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by knightdev on 5/24/17.
 */

public interface RequestInterfaceList {

//    @GET("api/water_level")
//    Call<List<LocationData>> getJSON();
    @GET("bins/qfqgn")
    Call<List<LocationData>> getJSON();


}
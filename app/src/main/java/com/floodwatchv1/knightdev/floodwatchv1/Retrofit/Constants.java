package com.floodwatchv1.knightdev.floodwatchv1.Retrofit;

/**
 * Created by knightdev on 5/9/17.
 */

public class Constants {

//    public static final String BASE_URL = "http://192.168.43.156:3000/";

    public static final String BASE_URL = "https://api.myjson.com/";
    public static final String REGISTER_OPERATION = "register";
    public static final String LOGIN_OPERATION = "login";
    public static final String CHANGE_PASSWORD_OPERATION = "chgPass";
    public static final String UPDATE_PROFILE_OPERATION = "updtProf";

    public static final String SUCCESS = "success";
    public static final String FAILURE = "failed";
    public static final String IS_LOGGED_IN = "isLoggedIn";

    public static final String LASTNAME = "last_name";
    public static final String FIRSTNAME = "first_name";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String UNIQUE_ID = "unique_id";
    public static final String TOKEN = "token";

    public static final String TAG = "Debugged!";
}

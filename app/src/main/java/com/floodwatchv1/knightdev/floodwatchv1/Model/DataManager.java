package com.floodwatchv1.knightdev.floodwatchv1.Model;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.floodwatchv1.knightdev.floodwatchv1.BuildConfig;
import com.floodwatchv1.knightdev.floodwatchv1.Controller.Login;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Constants;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Interfaces;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.RequestInterface;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerRequest;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerResponse;
//import com.retrofitsample.knightdev.retrofittest1.model.verssion.AndroidVersion2;
//import com.retrofitsample.knightdev.retrofittest1.retrofit.Interfaces.getVersionCallBack;
//import com.retrofitsample.knightdev.retrofittest1.retrofit.Interfaces.putVersionCallBack;
//import com.retrofitsample.knightdev.retrofittest1.retrofit.ServiceAPI;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by knightdev on 5/5/17.
 */

public class DataManager {
    private static DataManager mInstance;
    private Context context;
    private static Factory factory;
    private static RequestInterface serviceAPI;
    private static String TAG;

    public static DataManager getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new DataManager();
            mInstance.context = ctx;
            factory = new Factory();
            serviceAPI = factory.getInstance(mInstance.context);
            TAG = ctx.getClass().getSimpleName().toString();
        }
        return mInstance;
    }
//    public void registerProcess(ServerRequest request,final Interfaces.RegisterCallback callback){
//        Call<ServerResponse> callVersion=  serviceAPI.operation(request);
//
//        callVersion.enqueue(new Callback<ServerResponse>() {
//            @Override
//            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
//                if (response.isSuccessful()){
//                    callback.onResponse(true,response.body(),"Success");
//                }else {
//                    callback.onResponse(false,null,"Failed");
//
//                }
//            }
//            @Override
//            public void onFailure(Call<ServerResponse> call, Throwable t) {
//                callback.onResponse(false,null,"Failed");
//
//            }
//        });
//    }
//    public void loginProcess(ServerRequest request,final Interfaces.LoginCallback callback){
//        Call<ServerResponse> callVersion=  serviceAPI.operation(request);
//
//        callVersion.enqueue(new Callback<ServerResponse>() {
//            @Override
//            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
//                if (response.isSuccessful()){
//                    callback.onResponse(true,response.body(),"Success");
//                }else {
//                    callback.onResponse(false,null,"Failed");
//
//                }
//            }
//            @Override
//            public void onFailure(Call<ServerResponse> call, Throwable t) {
//                callback.onResponse(false,null,"Failed");
//
//            }
//        });
//    }
//    public void changePasswordProcess(ServerRequest request,final Interfaces.ChangePassCallback callback){
//        Call<ServerResponse> callVersion=  serviceAPI.operation(request);
//
//        callVersion.enqueue(new Callback<ServerResponse>() {
//            @Override
//            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
//                if (response.isSuccessful()){
//                    callback.onResponse(true,response.body(),"Success");
//                }else {
//                    callback.onResponse(false,null,response.errorBody().toString());
//
//                }
//            }
//            @Override
//            public void onFailure(Call<ServerResponse> call, Throwable t) {
//                callback.onResponse(false,null,t.getLocalizedMessage());
//            }
//        });
//    }
//    public void UpdateProfileProcess(ServerRequest request,final Interfaces.UpdateProfileCallback callback){
//        Call<ServerResponse> callVersion=  serviceAPI.operation(request);
//
//        callVersion.enqueue(new Callback<ServerResponse>() {
//            @Override
//            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
//                if (response.isSuccessful()){
//                    callback.onResponse(true,response.body(),"Success");
//                }else {
//                    callback.onResponse(false,null,response.errorBody().toString());
//
//                }
//            }
//            @Override
//            public void onFailure(Call<ServerResponse> call, Throwable t) {
//                callback.onResponse(false,null,t.getLocalizedMessage());
//            }
//        });
//    }
}

package com.floodwatchv1.knightdev.floodwatchv1.Controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.floodwatchv1.knightdev.floodwatchv1.Model.SessionManager;
import com.floodwatchv1.knightdev.floodwatchv1.R;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Constants;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by efc1980 on 5/29/2017.
 */

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {
    private static final String RESET_URL = "http://192.168.43.156:3000/api/users/forgot";

    public static final String KEY_EMAIL = "email";

    private EditText et_email;

    private Button btn_reset;
    AlertDialog.Builder builder;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pasword);

        et_email = (EditText) findViewById(R.id.et_email);

        btn_reset = (Button) findViewById(R.id.btn_reset);

        btn_reset.setOnClickListener(this);
    }

    private void resetPassword() {

        final String email = et_email.getText().toString().trim();

        if (!email.isEmpty()) {
            final ProgressDialog loading = ProgressDialog.show(this, "Connecting...", "Please wait...", false, false);

            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.readTimeout(60, TimeUnit.SECONDS);
            builder.connectTimeout(60, TimeUnit.SECONDS);
            builder.writeTimeout(60, TimeUnit.SECONDS);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, RESET_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            loading.dismiss();
                            try {
                                JSONObject server_response = new JSONObject(response);

                                String message = server_response.getString("message");
                                String result = server_response.getString("result");

                                if (message.equals("Email Sent!")) {
                                    if (result.equals(Constants.SUCCESS)) {
                                        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(ForgotPassword.this, "Server Error", Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            loading.dismiss();
                            Toast.makeText(ForgotPassword.this, "Connection/Server Error", Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(KEY_EMAIL, email);
                    params.put("Content-Type", "application/json");
                    params.put("Token", "");
                    return params;
                }

            };
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "Parameter should not be empty !", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_reset) {
            resetPassword();
            et_email.setText("");
        }
    }

    private void displayAlert(final String message) {
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}

package com.floodwatchv1.knightdev.floodwatchv1.Model;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.floodwatchv1.knightdev.floodwatchv1.Controller.ChangePassword;
import com.floodwatchv1.knightdev.floodwatchv1.Controller.MapAndList;

/**
 * Created by knightdev on 5/23/17.
 */

public class Communicator extends FragmentPagerAdapter {
    public Communicator(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new MapAndList();
            case 1:
                return new ChangePassword();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}

package com.floodwatchv1.knightdev.floodwatchv1.Controller;


import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.floodwatchv1.knightdev.floodwatchv1.BuildConfig;
import com.floodwatchv1.knightdev.floodwatchv1.Model.DataManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.SessionManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.SharedPrefManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.User;
import com.floodwatchv1.knightdev.floodwatchv1.R;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Constants;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Interfaces;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.RequestInterface;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerRequest;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class UpdateProfile extends Fragment implements View.OnTouchListener {
    TextView label;
    EditText et_email, et_username, et_firstname, et_lastname;
    String email = "", username = "", first_name = "", last_name = "";
    String emailCon = "", lastnameCon = "", firstnameCon = "", usernameCon = "", uniqueIDCon = "",
            passwordCon = "", token = "";
    Button save;
    AlertDialog.Builder builder;
    private ProgressDialog loading;
    SessionManager session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.update_profile, container, false);

        builder = new AlertDialog.Builder(getActivity());
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Sanchez-Regular.ttf");
        label = (TextView)view.findViewById(R.id.tv_title_lbl3);

        et_email = (EditText)view.findViewById(R.id.et_email);
        et_username = (EditText)view.findViewById(R.id.et_username);
        et_firstname = (EditText)view.findViewById(R.id.et_firstname);
        et_lastname = (EditText)view.findViewById(R.id.et_lastname);

        et_email.setText(emailCon);
        et_username.setText(usernameCon);
        et_firstname.setText(firstnameCon);
        et_lastname.setText(lastnameCon);

        save = (Button)view.findViewById(R.id.btn_save);

        label.setTypeface(face);

        save.setOnTouchListener(this);
        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();

        emailCon=pref.getString("email", null); // getting String
        token=pref.getString("token", null);

        return view;

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        email = et_email.getText().toString();
        username = et_username.getText().toString();
        first_name = et_firstname.getText().toString();
        last_name = et_lastname.getText().toString();

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (v.getId() == R.id.btn_save) {

                    if (last_name.isEmpty() || first_name.isEmpty() || username.isEmpty() || email.isEmpty()) {
                        Toast.makeText(getActivity(), "Parameters should not be empty !", Toast.LENGTH_LONG).show();
                    } else {
                        profileUpdate(last_name, first_name, username, emailCon, token);
                    }

//                    if (password.equals(finalPassword)){
//                        Toast.makeText(getApplicationContext(),"Passwords does not match",Toast.LENGTH_LONG).show();
//
//                    }else{
//                        Toast.makeText(getApplicationContext(),"Passwords does not match",Toast.LENGTH_LONG).show();
//                    }
                    //.Toast.makeText(getApplicationContext(),"Sample!",Toast.LENGTH_LONG).show()
                    //;
                    //UpdateProcess(uniqueIDCon,email,username);
                    //*loading = ProgressDialog.show(this,"Connecting...","Please wait...",false,false);

                   /* User user = new User();
                    user.setUniqueID(uniqueIDCon);
                    user.setNewEmail(email);
                    user.setNewUsername(username);
                    ServerRequest request = new ServerRequest();
                    request.setOperation(Constants.UPDATE_PROFILE_OPERATION);
                    request.setUser(user);
                    DataManager.getInstance(this).UpdateProfileProcess(request,
                            new Interfaces.UpdateProfileCallback() {
                                @Override
                                public void onResponse(boolean isSucess, ServerResponse response, String message) {
                                    if (response.getMessage().equals("Profile Updated Successfully")){
                                        if(response.getResult().equals(Constants.SUCCESS)){
                                            loading.dismiss();
                                            Toast.makeText(getApplicationContext(),"Username Updated!",Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(),GoogleMap.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("email",emailCon);
                                            bundle.putString("lastname", lastnameCon);
                                            bundle.putString("firstname", firstnameCon);
                                            bundle.putString("username", usernameCon);
                                            bundle.putString("password", passwordCon);
                                            bundle.putString("uniqueID", uniqueIDCon);
                                            intent.putExtras(bundle);
                                            startActivity(intent);
                                            finish();
                                        }
                                    } else{
                                        displayAlert(response.getMessage().toString());
                                    }
                                }
                            });

                    */

                }
        }
        return true;
    }

    private void profileUpdate(String last_name, String first_name, String username, String email,String le_token) {
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Connecting...", "Please wait...", false, false);

        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);

        //builder.certificatePinner(new CertificatePinner.Builder().add("*.androidadvance.com", "sha256/RqzElicVPA6LkKm9HblOvNOUqWmD+4zNXcRb+WjcaAE=")
        //    .add("*.xxxxxx.com", "sha256/8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8=")
        //    .add("*.xxxxxxx.com", "sha256/Ko8tivDrEjiY90yGasP6ZpBU4jwXvHqVvQI0GS3GNdA=")
        //    .add("*.xxxxxxx.com", "sha256/VjLZe/p3W/PJnd6lL8JVNBCGQBZynFLdZSTIqcO0SJ8=")
        //    .build());

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(getActivity().getCacheDir(), cacheSize);
        builder.cache(cache);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);

        User user = new User();
        user.setEmail(email);
        user.setUserName(username);
        user.setFirstName(first_name);
        user.setLastName(last_name);
        //user.setPassword(password);
        //user.setToken(le_token);

        ServerRequest request = new ServerRequest();
        request.setEmail(email);
        request.setUsername(username);
        request.setFirstname(first_name);
        request.setLastname(last_name);
        //request.setOperation(Constants.REGISTER_OPERATION);
        //request.setUser(user);

        Call<ServerResponse> response = requestInterface.profile("application/json", le_token, request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {

                ServerResponse resp = response.body();
                //Toast.makeText(getApplicationContext(),resp.getMessage() ,Toast.LENGTH_SHORT).show();
                //Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();
                //progress.setVisibility(View.INVISIBLE);
                if (resp.getMessage().equals("Profile Updated!")) {
                    if (resp.getResult().equals(Constants.SUCCESS)) {
                        //Toast.makeText(getApplicationContext(),resp.getMessage() ,Toast.LENGTH_SHORT).show();
                        //Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();
                        //progress.setVisibility(View.INVISIBLE);
                        if (resp.getMessage().equals("Profile Updated Successfully")) {
                            if (resp.getResult().equals(Constants.SUCCESS)) {
                                loading.dismiss();
                                Toast.makeText(getActivity(), "Username Updated!", Toast.LENGTH_SHORT).show();
                                session.logoutUser();

                            }
                        } else {
                            loading.dismiss();
                            Toast.makeText(getActivity(), resp.getMessage().toString(), Toast.LENGTH_SHORT).show();

                        }
                        //progress.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), resp.getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), Login.class));
                    }
                } else {
                    loading.dismiss();
                    Toast.makeText(getActivity(), resp.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
                loading.dismiss();
                //progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                //progress.setVisibility(View.INVISIBLE);
                Log.d(Constants.TAG, "failed");
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
                //Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

            }
        });
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_items, menu);
//        return true;
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            // action with ID action_refresh was selected
//            case R.id.action_change_password:
//                Intent intent = new Intent(getApplicationContext(),ChangePassword.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("email",emailCon);
//                bundle.putString("last_name", lastnameCon);
//                bundle.putString("first_name", firstnameCon);
//                bundle.putString("username", usernameCon);
//                bundle.putString("password", passwordCon);
//                bundle.putString("uniqueID", uniqueIDCon);
//                bundle.putString("token", token);
//                intent.putExtras(bundle);
//                startActivity(intent);
//                finish();
//                break;
//            // action with ID action_settings was selected
//            case R.id.action_map:
//                intent = new Intent(getApplicationContext(),GoogleMap.class);
//                bundle = new Bundle();
//                bundle.putString("email",emailCon);
//                bundle.putString("last_name", lastnameCon);
//                bundle.putString("first_name", firstnameCon);
//                bundle.putString("username", usernameCon);
//                bundle.putString("password", passwordCon);
//                bundle.putString("uniqueID", uniqueIDCon);
//                bundle.putString("token", token);
//                intent.putExtras(bundle);
//                startActivity(intent);
//                finish();
//                break;
//            case R.id.action_logOut:
//                startActivity(new Intent(getApplicationContext(), Login.class));
//                finish();
//                break;
//            default:
//                break;
//        }
//
//        return true;
//    }
//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        menu.findItem(R.id.action_update_profile).setVisible(false);
//        super.onPrepareOptionsMenu(menu);
//        return true;
//    }

    /*private void UpdateProcess(String unique_id,String newEmail,String newUsername){
        final ProgressDialog loading = ProgressDialog.show(this,"Connecting...","Please wait...",false,false);

        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);

        //builder.certificatePinner(new CertificatePinner.Builder().add("*.androidadvance.com", "sha256/RqzElicVPA6LkKm9HblOvNOUqWmD+4zNXcRb+WjcaAE=")
        //    .add("*.xxxxxx.com", "sha256/8Rw90Ej3Ttt8RRkrg+WYDS9n7IS03bk5bjP/UXPtaY8=")
        //    .add("*.xxxxxxx.com", "sha256/Ko8tivDrEjiY90yGasP6ZpBU4jwXvHqVvQI0GS3GNdA=")
        //    .add("*.xxxxxxx.com", "sha256/VjLZe/p3W/PJnd6lL8JVNBCGQBZynFLdZSTIqcO0SJ8=")
        //    .build());

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        int cacheSize = 30 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(this.getCacheDir(), cacheSize);
        builder.cache(cache);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);

        User user = new User();
        user.setUniqueID(unique_id);
        user.setNewEmail(newEmail);
        user.setNewUsername(newUsername);
        ServerRequest request = new ServerRequest();
        request.setOperation(Constants.UPDATE_PROFILE_OPERATION);
        request.setUser(user);
        Call<ServerResponse> response = requestInterface.operation(request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {

                ServerResponse resp = response.body();
                //Toast.makeText(getApplicationContext(),resp.getMessage() ,Toast.LENGTH_SHORT).show();
                //Snackbar.make(getView(), resp.getMessage(), Snackbar.LENGTH_LONG).show();
                //progress.setVisibility(View.INVISIBLE);
                if (resp.getMessage().equals("Profile Updated Successfully")){
                    if(resp.getResult().equals(Constants.SUCCESS)){
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(),"Username Updated!",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(),GoogleMap.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("email",emailCon);
                        bundle.putString("lastname", lastnameCon);
                        bundle.putString("firstname", firstnameCon);
                        bundle.putString("username", usernameCon);
                        bundle.putString("password", passwordCon);
                        bundle.putString("uniqueID", uniqueIDCon);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                } else{
                    displayAlert(resp.getMessage().toString());
                }
                //progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                //progress.setVisibility(View.INVISIBLE);
                Log.d(Constants.TAG,"failed");
                loading.dismiss();
                Toast.makeText(getApplicationContext(),t.getLocalizedMessage() ,Toast.LENGTH_SHORT).show();
                //Snackbar.make(getView(), t.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();

            }
        });
    }*/


}

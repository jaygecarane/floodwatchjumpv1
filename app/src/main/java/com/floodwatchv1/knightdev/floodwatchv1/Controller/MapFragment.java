package com.floodwatchv1.knightdev.floodwatchv1.Controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.floodwatchv1.knightdev.floodwatchv1.MainContent;
import com.floodwatchv1.knightdev.floodwatchv1.Model.CustomMessageEvent;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.MySingleton;
import com.floodwatchv1.knightdev.floodwatchv1.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by JohnChristopher on 2016-10-06.
 */

public class MapFragment extends SupportMapFragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.InfoWindowAdapter,
        GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback {
    private GoogleApiClient mGoogleApiClient;
    private Location mCurrentLocation;

    private final String message = "";
    private final int[] MAP_TYPES = {GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE};
    private int curMapTypeIndex = 1;
    private GoogleMap googleMap;
    MapFragment mapFragment;
    AlertDialog.Builder builder;


    String location = "";
    String load_locations = "http://api.myjson.com/bins/qfqgn";

    //    String fetch_locations = "http://webbackendlearning.hol.es/forMap/fetch_location_details.php";
    String locName = "";
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EventBus.getDefault().register(this);
        setHasOptionsMenu(true);

        builder = new AlertDialog.Builder(getActivity());

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        try {

            if (googleMap == null) {
                mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapB));
                mapFragment.getMapAsync(this);
                initListeners();

            }

        } catch (Exception e) {

            e.printStackTrace();
        }


    }

    private void initListeners() {
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mCurrentLocation = LocationServices
                .FusedLocationApi
                .getLastLocation(mGoogleApiClient);

//        if (mCurrentLocation.toString().isEmpty()){
//            displayAlert("Please check your internet connection or location services");
//        }else {
        if (mCurrentLocation == null) {
            //displayAlert("location_error");
            //Toast.makeText(getActivity(), "Could not pin location.Please check your location services if turned on", Toast.LENGTH_SHORT).show();
        } else {
            initCamera(mCurrentLocation);
            getRegisteredLocations();
        }
        //}
    }

    private void initCamera(Location location) {
        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(location.getLatitude(),
                        location.getLongitude()))
                .zoom(16f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();
        LatLng fetchedLoc = new LatLng(Double.parseDouble(String.valueOf(location.getLatitude())), Double.parseDouble(String.valueOf(location.getLongitude())));

        //int strokeColor = 0xffff0000;
        String string = "#add8e6";
        int strokeColor = Integer.parseInt(string.replaceFirst("^#", ""), 16);
        //opaque red fill
        int shadeColor = 0x44ff0000;
        Circle circle = googleMap.addCircle(new CircleOptions()
                .center(fetchedLoc)
                .radius(1000)
                .strokeColor(Color.parseColor("#500084d3"))
                .fillColor(Color.parseColor("#500084d3")));

        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), null);

        googleMap.setMapType(MAP_TYPES[curMapTypeIndex]);
        googleMap.setTrafficEnabled(true);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Subscribe
    public void onEvent(CustomMessageEvent event) {
        location = event.getCustomMessage();
        location.toString();
        List<Address> addresslist = null;
        Geocoder geocoder = new Geocoder(getActivity());
        if (location != null || location.equals("")) {
            try {
                addresslist = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Address address = addresslist.get(0);
            LatLng latlng = new LatLng(address.getLatitude(), address.getLongitude());
            Toast.makeText(getActivity(), "Travelling to " + location, Toast.LENGTH_SHORT).show();
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latlng));
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return true;
    }

    public void getRegisteredLocations() {
        //final ProgressDialog loading = ProgressDialog.show(getActivity(), "Loading Locations...", "Please wait...", false, false);
        StringRequest strReq = new StringRequest(load_locations, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray location = new JSONArray(response);
                    //  Log.d("Try", deviceID.toString());
                    for (int i = 0; i < location.length(); i++) {
                        JSONObject jsonObject = location.getJSONObject(i);
                        String locationName = jsonObject.getString("name");
                        String latitude = jsonObject.getString("latitude");
                        String longitude = jsonObject.getString("longitude");
                        LatLng fetchedLoc = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                        MarkerOptions options = new MarkerOptions().position(fetchedLoc);
                        options.title(locationName);
//                            options.snippet(deviceID);
//                            if (status.equals("Evacuate")) {
                        options.icon(BitmapDescriptorFactory.fromBitmap(
                                BitmapFactory.decodeResource(getResources(),
                                        R.mipmap.monitor_level)));
//                            }else if (status.equals("Prepare")){
//                                options.icon(BitmapDescriptorFactory.fromBitmap(
//                                        BitmapFactory.decodeResource(getResources(),
//                                                R.mipmap.prepare_level)));
//                            }else {
//                                options.icon(BitmapDescriptorFactory.fromBitmap(
//                                        BitmapFactory.decodeResource(getResources(),
//                                                R.mipmap.monitor_level)));
//                            }

                        googleMap.addMarker(options);
                    }
                    //      loading.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("error", e.toString());
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        // loading.dismiss();
                        Toast.makeText(getActivity(), "Unable to load locations , please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(getActivity()).addToRequestque(strReq);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        locName = marker.getTitle().toString();
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Gathering location details", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(load_locations,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            loading.dismiss();
                            JSONArray jsonArray = new JSONArray(response);


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                JSONObject jsonDevice = jsonObject.getJSONObject("device");
                                String strDevice = jsonDevice.getString("name");
                                JSONObject jsonLevel = jsonDevice.getJSONObject("level");
                                String strLevelStatus = jsonLevel.getString("status");
                                String strLevelUpdatedAt = jsonLevel.getString("updated_at");
                                if (locName.equals(jsonObject.getString("name"))) {
                                    Intent intent = new Intent(getActivity(), MainContent.class);
                                    Bundle bundle_info = new Bundle();
                                    bundle_info.putString("locationName", locName);
                                    bundle_info.putString("deviceID", strDevice);
                                    bundle_info.putString("status", strLevelStatus);
                                    bundle_info.putString("dateTime", strLevelUpdatedAt);
                                    intent.putExtras(bundle_info);
                                    startActivity(intent);
                                    break;
                                } else {
                                    continue;
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Toast.makeText(getActivity(), "Connection Error", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }) {
        };
        MySingleton.getInstance(getActivity()).addToRequestque(stringRequest);
    }


    private void displayAlert(final String message) {
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        googleMap.setTrafficEnabled(true);
        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

    }
}







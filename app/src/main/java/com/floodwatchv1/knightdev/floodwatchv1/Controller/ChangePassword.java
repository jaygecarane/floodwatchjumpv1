package com.floodwatchv1.knightdev.floodwatchv1.Controller;


import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.floodwatchv1.knightdev.floodwatchv1.BuildConfig;
import com.floodwatchv1.knightdev.floodwatchv1.Model.DataManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.SessionManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.SharedPrefManager;
import com.floodwatchv1.knightdev.floodwatchv1.Model.User;
import com.floodwatchv1.knightdev.floodwatchv1.R;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Constants;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.Interfaces;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.RequestInterface;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerRequest;
import com.floodwatchv1.knightdev.floodwatchv1.Retrofit.ServerResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class ChangePassword extends Fragment implements View.OnTouchListener{
    EditText et_newPassword, et_oldPassword, et_confirmNewPassword;
    SessionManager session;
    Button save;
    TextView label;
    String emailCon,token;
    AlertDialog.Builder builder;
    ProgressDialog loading;
    public ChangePassword(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.change_password, container, false);
        builder = new AlertDialog.Builder(getActivity());
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Sanchez-Regular.ttf");
        label = (TextView)view.findViewById(R.id.tv_title_lbl3);
        et_oldPassword = (EditText)view.findViewById(R.id.et_oldPassword);
        et_newPassword = (EditText)view.findViewById(R.id.et_newPassword);
        et_confirmNewPassword = (EditText)view.findViewById(R.id.et_confirmNewPassword);

        save = (Button)view.findViewById(R.id.btn_save);

        label.setTypeface(face);
        save.setOnTouchListener(this);

        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();

        emailCon=pref.getString("email", null); // getting String
        token=pref.getString("token", null);
        return view;

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        String old_password = et_oldPassword.getText().toString();
        String new_password = et_newPassword.getText().toString();
        String confirm_password = et_confirmNewPassword.getText().toString();
//        Toast.makeText(this,passwordCon+"\n"+lastnameCon+"\n"+firstnameCon+"\n"+emailCon+"\n",
//                Toast.LENGTH_SHORT).show();
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (v.getId() == R.id.btn_save) {
                    if (!old_password.isEmpty() && !new_password.isEmpty()) {
                        if (new_password.equals(confirm_password)) {
                            changePasswordProcess(emailCon, old_password, new_password,confirm_password);
                        } else {
                            Toast.makeText(getActivity(), "Passwords does not match", Toast.LENGTH_SHORT).show();
                        }
                        //changePasswordProcess(emailCon,old_password,new_password);
                        /*User user = new User();
                        user.setEmail(emailCon);
                        user.setOld_password(old_password);
                        user.setNew_password(new_password);
                        ServerRequest request = new ServerRequest();
                        request.setOperation(Constants.CHANGE_PASSWORD_OPERATION);
                        request.setUser(user);
                        DataManager.getInstance(this).changePasswordProcess(request,
                                new Interfaces.ChangePassCallback() {
                                    @Override
                                    public void onResponse(boolean isSuccess, ServerResponse response, String message) {
                                        if (isSuccess) {
                                            ServerResponse resp = response;

                                            if(resp.getResult().equals(Constants.SUCCESS)){
                                                loading.dismiss();
                                                Toast.makeText(getApplicationContext(),resp.getMessage(),Toast.LENGTH_SHORT).show();
                                            }else {
                                                loading.dismiss();
                                                Toast.makeText(getApplicationContext(),resp.getMessage(),Toast.LENGTH_SHORT).show();
                                            }
                                            loading.dismiss();
                                            Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                                        } else {
                                            loading.dismiss();

                                            Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });*/

                    } else {
                        Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }
        }
        return true;
    }

    private void changePasswordProcess(String email, String old_password, String new_password, String new_password_confirmation) {
   // private void changePasswordProcess(String email,String old_password,String new_password){
        final ProgressDialog loading = ProgressDialog.show(getActivity(),"Connecting...","Please wait...",false,false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestInterface requestInterface = retrofit.create(RequestInterface.class);
        User user = new User();
        user.setEmail(email);
        user.setPassword(old_password);
        user.setNewpassword(new_password);
        user.setNewpassword_confirmation(new_password_confirmation);

        ServerRequest request = new ServerRequest();
        //request.setOperation(Constants.CHANGE_PASSWORD_OPERATION);
        //request.setUser(user);
        request.setEmail(email);
        request.setPassword(old_password);
        request.setNewpassword(new_password);
        request.setNewpassword_confirmation(new_password_confirmation);


        Call<ServerResponse> response = requestInterface.password("application/json", token, request);

        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {

                ServerResponse resp = response.body();

                if(resp.getResult().equals(Constants.SUCCESS)){
                    loading.dismiss();

                    Toast.makeText(getActivity(),resp.getMessage(),Toast.LENGTH_SHORT).show();
                }else {
                    loading.dismiss();
                    Toast.makeText(getActivity(),resp.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(getActivity(),t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


}

